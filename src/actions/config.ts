import { ActionTypes } from "./types";
import config from "../assets/config.json";
import { LoadConfigActionInterface } from "../interfaces/LoadConfigActionInterface";

export const loadConfig = (): LoadConfigActionInterface => {
  return {
    type: ActionTypes.loadConfig,
    payload: config,
  };
};
