import { ActionTypes } from "./types";
import phrases from "../assets/phrases.json";
import config from "../assets/config.json";
import { LoadPhrasesActionInterface } from "../interfaces/LoadPhrasesActionInterface";

export const loadPhrases = (): LoadPhrasesActionInterface => {
  // sort values by random order, that way the card will be more dynamic
  let returnedPhrases = phrases
    .map((value) => ({ value, sort: Math.random() }))
    .sort((a, b) => a.sort - b.sort)
    .map(({ value }) => value);

  // find were the array should be split
  // -1 since array starts from 0
  const middleGrounds =
    Math.round((config.numColumns * config.rowLetters.length) / 2) - 1;

  // separate bingo phrases to 3 separate arrays and join them
  const firstArray = returnedPhrases.slice(0, middleGrounds);
  const secondArray = returnedPhrases.slice(middleGrounds);
  const bingoPhrase = [config.bingoCardText];

  const arrayToReturn = [...firstArray, ...bingoPhrase, ...secondArray];

  return {
    type: ActionTypes.loadPhrases,
    payload: arrayToReturn,
  };
};
