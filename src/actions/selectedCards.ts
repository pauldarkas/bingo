import { SetSelectedCardsInterface } from "../interfaces/SetSelectedCardsInterface";
import { ActionTypes } from "./types";

export const setSelectedCards = (
  row: string,
  column: number[]
): SetSelectedCardsInterface => {
  return {
    type: ActionTypes.selectedCards,
    payload: { row, column },
  };
};
