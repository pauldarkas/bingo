import { LoadConfigActionInterface } from "../interfaces/LoadConfigActionInterface";
import { LoadPhrasesActionInterface } from "../interfaces/LoadPhrasesActionInterface";
import { SetSelectedCardsInterface } from "../interfaces/SetSelectedCardsInterface";

export enum ActionTypes {
  loadPhrases,
  loadConfig,
  selectedCards,
}

export type Action = LoadPhrasesActionInterface | LoadConfigActionInterface;
export type CardAction = SetSelectedCardsInterface;
export type nullOrString = null | string;
