import { useEffect, useState } from "react";
import { CardTable } from "./CardTable";
import { useSelector, shallowEqual } from "react-redux";
import { useDispatch } from "react-redux";
import { loadPhrases, loadConfig, nullOrString } from "../actions";
import { StoreStateInterface } from "../interfaces/StoreStateInterface";
import "../assets/styles/App.css";

export const App = (): JSX.Element => {
  const dispatch = useDispatch();

  // set define state values and methods
  const [isBingoRow, setIsBingoRow] = useState(null as nullOrString);
  const [isBingoCol, setIsBingoCol] = useState(null as null | number);
  const [isBingoDiag, setIsBingoDiag] = useState(null as nullOrString);

  // get values from redux
  const { selectedCards } = useSelector(
    (state: StoreStateInterface) => ({
      selectedCards: state.selectedCards,
    }),
    shallowEqual
  );

  // triggered on selectedCards change
  useEffect(() => {
    // temp variables fro bingo logic
    let bingoRow = false;
    let bingoCol = false;
    let bingoDiag = false;
    let numBingoDiagLeft = 0;
    let numBingoDiagRight = 0;
    let colArray = [] as string[][];

    // based on interface, selected cards is multidimensional array with letter as a key and number array as value
    for (const cardRow in selectedCards) {
      const cardRowArray: [] = selectedCards[cardRow] as [];

      // not the ideal logic, but used to check how many selected values each letter has
      // c is exception since it is in the middle and possible selected values can only be 4
      if (
        (cardRowArray.length.valueOf() === 5 && cardRow !== "c") ||
        (cardRowArray.length.valueOf() === 4 && cardRow === "c")
      ) {
        bingoRow = true;
        setIsBingoRow(cardRow);
      }

      // loop through second array to check if column has bingo
      for (const cardCol of cardRowArray) {
        // mutate the array if is not set, set it, if is set, push the value
        if (!colArray[cardCol]) {
          colArray[cardCol] = [cardRow];
        } else {
          colArray[cardCol].push(cardRow);
        }

        // bad logic, should compare array keys from config and letter positions
        if (
          (cardRow === "a" && cardCol === 0) ||
          (cardRow === "b" && cardCol === 1) ||
          (cardRow === "d" && cardCol === 3) ||
          (cardRow === "e" && cardCol === 4)
        ) {
          // increase the left value by one if it matches the clause
          numBingoDiagLeft++;
        }

        // same problem as the above one
        if (
          (cardRow === "e" && cardCol === 0) ||
          (cardRow === "d" && cardCol === 1) ||
          (cardRow === "b" && cardCol === 3) ||
          (cardRow === "a" && cardCol === 4)
        ) {
          // increase the right value by one if it matches the clause
          numBingoDiagRight++;
        }
      }
    }

    // if 4 options were selected, we have bingo, middle one is excluded since it is not possible to select it
    if (numBingoDiagLeft === 4) {
      bingoDiag = true;
      setIsBingoDiag("left");
    }

    // same logic, but for right
    if (numBingoDiagRight === 4) {
      bingoDiag = true;
      setIsBingoDiag("right");
    }

    // loop trough temp array that was just created
    for (const key in colArray) {
      const column = colArray[key];

      // not the best logic, would be better to check based on config
      // similar in sense as row, but just for numbers
      if (
        (column.length.valueOf() === 5 && key !== "2") ||
        (column.length.valueOf() === 4 && key === "2")
      ) {
        bingoCol = true;
        setIsBingoCol(Number(key));
      }
    }

    // logic in case card was unselected
    if (!bingoRow) {
      setIsBingoRow(null);
    }

    if (!bingoCol) {
      setIsBingoCol(null);
    }

    if (!bingoDiag) {
      setIsBingoDiag(null);
    }
  }, [selectedCards]);

  // dispatch actions for redux. Loading phrases and config
  useEffect(() => {
    dispatch(loadPhrases());
    dispatch(loadConfig());
  }, [dispatch]);

  // return component
  return (
    <div className="container">
      {isBingoRow || isBingoCol !== null || isBingoDiag ? (
        <div className="victory-text">You won!!!</div>
      ) : (
        <div className="victory-space" />
      )}
      <CardTable
        bingoDiag={isBingoDiag}
        bingoCol={isBingoCol}
        bingoRow={isBingoRow}
      />
    </div>
  );
};
