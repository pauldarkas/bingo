// middle bingo card
export const BingoCard = ({ phrase }: { phrase: string }): JSX.Element => {
  return (
    <div className="bingo-card">
      <div className="bing-card-body">{phrase}</div>
    </div>
  );
};
