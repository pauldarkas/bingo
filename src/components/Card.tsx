import { useState } from "react";
import { useDispatch } from "react-redux";
import { useSelector, shallowEqual } from "react-redux";
import { setSelectedCards } from "../actions";
import { CardInterface } from "../interfaces/CardInterface";
import { StoreStateInterface } from "../interfaces/StoreStateInterface";

export const Card = ({
  rowLetter,
  columnNumber,
  phrase,
  isBingo,
}: CardInterface): JSX.Element => {
  // get required redux values
  const { selectedCards } = useSelector(
    (state: StoreStateInterface) => ({
      selectedCards: state.selectedCards,
    }),
    shallowEqual
  );

  const dispatch = useDispatch();
  const [selected, setSelected] = useState(false);

  const onClick = (isSelected: boolean): void => {
    setSelected(isSelected);

    // for typescript it is required to define this type otherwise an error will be thrown
    type ObjectKey = keyof typeof selectedCards;
    const selectedCardRowKey = rowLetter as ObjectKey;
    // store the array from redux to temp value
    const letterArray = (selectedCards[selectedCardRowKey] as []) || [];

    let assignedArray = [];

    if (isSelected) {
      // assign column number to temp array
      assignedArray = [...letterArray, columnNumber];
    } else {
      // or remove it if card was unclicked
      assignedArray = letterArray.filter((item) => item !== columnNumber);
    }

    // dispatch values to redux
    dispatch(setSelectedCards(rowLetter, assignedArray));
  };

  return (
    <div
      onClick={(): void => onClick(!selected)}
      className={`card ${selected ? "strike-out" : ""}  ${
        isBingo ? "card-bingo" : ""
      }`}
    >
      {phrase}
    </div>
  );
};
