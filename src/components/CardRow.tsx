import { useSelector, shallowEqual } from "react-redux";
import { StoreStateInterface } from "../interfaces/StoreStateInterface";
import { Card } from "./Card";
import { BingoCard } from "./BingoCard";
import { CardRowInterface } from "../interfaces/CardRowInterface";

export const CardRow = ({
  isBingoCard,
  rowLetter,
  rowId,
  isBingo,
  bingoDiag,
  bingoCol,
}: CardRowInterface): JSX.Element => {
  const { config, phrases } = useSelector(
    (state: StoreStateInterface) => ({
      config: state.config,
      phrases: state.phrases,
    }),
    shallowEqual
  );

  const renderRow = (): JSX.Element[] => {
    const cards = [];
    // calculate which array element it is based on row id
    // each row has 5 elements, so multiply it by 5
    let currentIndex = rowId * 5;

    for (let column = 0; column < config.numColumns; column++) {
      const cardBingo = checkIfBingo(
        bingoDiag,
        bingoCol,
        isBingo,
        rowLetter,
        column
      );
      // check if card is bingo or not
      // bingo card is blocked and has different styling
      if (column === 2 && isBingoCard) {
        cards.push(<BingoCard key={column} phrase={phrases[currentIndex]} />);
      } else {
        // push the cards array
        cards.push(
          <Card
            key={column}
            columnNumber={column}
            rowLetter={rowLetter}
            phrase={phrases[currentIndex]}
            isBingo={cardBingo}
          />
        );
      }

      // premature way of checking current index for phrases
      currentIndex++;
    }

    return cards;
  };

  const checkIfBingo = (
    bingoDiag: null | string,
    bingoCol: null | number,
    isBingo: boolean,
    rowLetter: string,
    column: number
  ): boolean => {
    // if is bingo, well it is bingo
    if (isBingo) {
      return true;
    }

    // have to check this one if not null, since 0 number is false fro truthy
    // also have to check if bingocol matches column id
    if (bingoCol !== null && bingoCol === column) {
      return true;
    }

    // check for bingo diag
    if (bingoDiag) {
      return bingoDiagSelected(bingoDiag, rowLetter, column);
    }

    // if nothing matched, just return false as default
    return false;
  };

  const bingoDiagSelected = (
    bingoDiag: string,
    rowLetter: string,
    column: number
  ): boolean => {
    // check for left or right
    if (bingoDiag === "left") {
      // again, bad way to check this, but for now it works
      if (
        (rowLetter === "a" && column === 0) ||
        (rowLetter === "b" && column === 1) ||
        (rowLetter === "d" && column === 3) ||
        (rowLetter === "e" && column === 4)
      ) {
        return true;
      }
    } else {
      if (
        (rowLetter === "e" && column === 0) ||
        (rowLetter === "d" && column === 1) ||
        (rowLetter === "b" && column === 3) ||
        (rowLetter === "a" && column === 4)
      ) {
        return true;
      }
    }

    return false;
  };

  return <div className="card-row">{renderRow()}</div>;
};
