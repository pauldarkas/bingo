import { CardRow } from "./CardRow";
import { useSelector, shallowEqual } from "react-redux";
import { StoreStateInterface } from "../interfaces/StoreStateInterface";
import { CardTableInterface } from "../interfaces/CardTableInterface";

export const CardTable = ({
  bingoDiag,
  bingoCol,
  bingoRow,
}: CardTableInterface): JSX.Element => {
  const { config } = useSelector(
    (state: StoreStateInterface) => ({
      config: state.config,
    }),
    shallowEqual
  );

  const renderRow = (): JSX.Element[] => {
    const cardsRow = [];
    let bingoCard = false;

    for (let row = 0; row < config.rowLetters.length; row++) {
      // we can check if this is bingo based on row values
      // compare bingo row to config row
      const isBingo = bingoRow !== null && bingoRow === config.rowLetters[row];
      // small probability that this is bingo card
      bingoCard = row === 2;
      cardsRow.push(
        <CardRow
          rowLetter={config.rowLetters[row]}
          rowId={row}
          isBingoCard={bingoCard}
          isBingo={isBingo}
          bingoDiag={bingoDiag}
          bingoCol={bingoCol}
          key={row}
        />
      );
    }

    return cardsRow;
  };

  return <div className="table">{renderRow()}</div>;
};
