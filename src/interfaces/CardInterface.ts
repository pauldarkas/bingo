export interface CardInterface {
  rowLetter: string;
  columnNumber: number;
  phrase: string;
  isBingo: boolean;
}
