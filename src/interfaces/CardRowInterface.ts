export interface CardRowInterface {
  isBingoCard: boolean;
  rowLetter: string;
  rowId: number;
  isBingo: boolean;
  bingoDiag: null | string;
  bingoCol: null | number;
}
