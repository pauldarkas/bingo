export interface CardTableInterface {
  bingoDiag: null | string;
  bingoCol: null | number;
  bingoRow: null | string;
}
