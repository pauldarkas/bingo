export interface ConfigReducerInterface {
  rowLetters: string[];
  numColumns: number;
  bingoCardText: string;
}
