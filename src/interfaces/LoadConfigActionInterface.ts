import { ConfigReducerInterface } from "./ConfigReducerInterface";
import { ActionTypes } from "../actions";

export interface LoadConfigActionInterface {
  type: ActionTypes.loadConfig;
  payload: ConfigReducerInterface;
}
