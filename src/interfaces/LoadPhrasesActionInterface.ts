import { ActionTypes } from "../actions";

export interface LoadPhrasesActionInterface {
  type: ActionTypes.loadPhrases;
  payload: string[];
}
