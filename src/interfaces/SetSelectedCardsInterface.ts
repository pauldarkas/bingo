import { ActionTypes } from "../actions";

export interface SetSelectedCardsInterface {
  type: ActionTypes.selectedCards;
  payload: { row: string; column: number[] };
}
