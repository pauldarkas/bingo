import { ConfigReducerInterface } from "./ConfigReducerInterface";

export interface StoreStateInterface {
  phrases: string[];
  config: ConfigReducerInterface;
  selectedCards: number[][];
}
