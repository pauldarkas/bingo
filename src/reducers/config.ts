import { Action, ActionTypes } from "../actions";
import { ConfigReducerInterface } from "../interfaces/ConfigReducerInterface";

export const configReducer = (
  state: ConfigReducerInterface = {
    rowLetters: [],
    numColumns: 0,
    bingoCardText: "",
  },
  action: Action
) => {
  switch (action.type) {
    case ActionTypes.loadConfig:
      return {
        ...state,
        rowLetters: action.payload.rowLetters,
        numColumns: action.payload.numColumns,
        bingoCardText: action.payload.bingoCardText,
      };
    default:
      return state;
  }
};
