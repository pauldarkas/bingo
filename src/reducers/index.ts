import { combineReducers } from "redux";
import { phrasesReducer } from "./phrases";
import { configReducer } from "./config";
import { selectedCardsReducer } from "./selectedCards";
import { StoreStateInterface } from "../interfaces/StoreStateInterface";

export const reducers = combineReducers<StoreStateInterface>({
  phrases: phrasesReducer,
  config: configReducer,
  selectedCards: selectedCardsReducer,
});
