import { Action, ActionTypes } from "../actions";

export const phrasesReducer = (state: string[] = [], action: Action) => {
  switch (action.type) {
    case ActionTypes.loadPhrases:
      return action.payload;
    default:
      return state;
  }
};
