import { CardAction, ActionTypes } from "../actions";

export const selectedCardsReducer = (
  state: number[][] = [],
  action: CardAction
) => {
  switch (action.type) {
    case ActionTypes.selectedCards:
      // set letter as array key and pass in column array of numbers
      // if array key exists, it will be overwritten with values from action
      return {
        ...state,
        [action.payload.row]: action.payload.column,
      };
    default:
      return state;
  }
};
